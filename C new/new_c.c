#include <stdio.h>
    struct student {
    char Name[50];
    float marks;
} s[10];

int main() {
    int i;
    printf("Enter information of students:\n");

    for (i = 0; i < 5; ++i) {
        printf("Enter name: ");
        scanf("%s", s[i].Name);
        printf("Enter marks:\n ");
        scanf("%f", &s[i].marks);
    }
    printf("Displaying Information:\n\n");

    for (i = 0; i < 5; ++i) {
        printf("Name: ");
        puts(s[i].Name);
        printf("Marks: %.1f", s[i].marks);
        printf("\n");
    }
    return 0;
}
